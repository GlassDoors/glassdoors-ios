//
//  GBMainViewController.swift
//  Glassbooks
//
//  Created by Abdullah Kahn on 2/8/17.
//  Copyright © 2017 Glassbooks. All rights reserved.
//

import UIKit

class GBMainViewController: UIViewController {
    
    @IBOutlet var carouselContainerView: UIView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var treeViewContainer: UIView!
    var treeViewController: GBTreeViewController?
    var carouselViewController: GBCarouselViewController?
    let treeViewSegueName = "treeSegue"
    let carouselSegueName = "carousalSegue"

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == carouselSegueName {
            carouselViewController = segue.destination as? GBCarouselViewController
        }
        else if segue.identifier == treeViewSegueName {
               treeViewController = segue.destination as? GBTreeViewController
        }
    }
    
    func treeDidUpdate(imageArray:NSArray) {
        self.carouselContainerView.isHidden = true
        imageView.image = UIImage(named: imageArray[0] as! String)
        
        if imageArray.count > 1 {
            self.carouselContainerView.isHidden = false
            //update carousel data source\
            carouselViewController?.imageArray.removeAllObjects()
            carouselViewController?.imageArray.addObjects(from: imageArray as! [Any])
            carouselViewController?.collectionView?.reloadData()
        }
    }
    
    func carouselDidUpdate(imageName:String) {
        imageView.image = UIImage(named: imageName)
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  }
