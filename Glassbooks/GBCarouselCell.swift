//
//  GBCarouselCell.swift
//  Glassbooks
//
//  Created by Abdullah Kahn on 2/8/17.
//  Copyright © 2017 Glassbooks. All rights reserved.
//

import UIKit

class GBCarouselCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
}
