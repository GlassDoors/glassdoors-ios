//
//  GBTreeViewController.swift
//  Glassbooks
//
//  Created by Abdullah Kahn on 2/8/17.
//  Copyright © 2017 Glassbooks. All rights reserved.
//

import UIKit
import SwiftyJSON
import RATreeView
import WebKit

class GBTreeViewController: UIViewController, RATreeViewDelegate, RATreeViewDataSource, CarousalDelegate {
    
    var treeView : RATreeView!
    var contentImageView: UIImageView!
    var data : [DataObject]
    var touchLabel : UILabel!
    
    var MainViewController: GBMainViewController?
    
    //MARK: Overrides & inits
    convenience init() {
        self.init(nibName : nil, bundle: nil)
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
    super.didMove(toParentViewController: parent)
    MainViewController = parent as? GBMainViewController
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        data = GBTreeViewController.parseJSON()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        data = GBTreeViewController.parseJSON()
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupContentView()
        setupTreeView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GBCarouselViewController.sharedInstance.delegate = self
    }
    
    //MARK: RATreeViewDatasource Methods
    public func treeView(_ treeView: RATreeView, cellForItem item: Any?) -> UITableViewCell {
        
        let cell = treeView.dequeueReusableCell(withIdentifier: "GBTreeViewCell") as! GBTreeViewCell
        let item = item as! DataObject
        
        let level = treeView.levelForCell(forItem: item)
        let detailsText = "Number of children \(item.children.count)"
        cell.selectionStyle = .none
        cell.setup(withTitle: item.name, detailsText: detailsText, level: level, additionalButtonHidden: false)
        
        var frame = CGRect()
        frame = cell.frame
        
        //only a bottomCell would have a topCellObject
        var originalImage = UIImage()
        let imageName = "\(item.name)-thumbnail"
        
        //cell is a bottom cell
        if !(item.topCellObject.isEmpty)  {
            //half size
            frame.size.height = cell.frame.size.height / 2
            originalImage = (UIImage(named: imageName)?.bottomHalf)!
        }
            //cell is a top cell
        else if item.children.count > 0 {
            frame.size.height = cell.frame.size.height / 2
            originalImage = (UIImage(named: imageName)?.topHalf)!
        }
            //cell has no children
        else { originalImage = (UIImage(named: imageName))!
        }
        
        cell.cellImageView.image = originalImage
        cell.cellImageView.backgroundColor = UIColor.clear
        cell.cellImageView.contentMode = UIViewContentMode.scaleAspectFit
        
        return cell
    }
    
    public func treeView(_ treeView: RATreeView, numberOfChildrenOfItem item: Any?) -> Int {
        if let item = item as? DataObject {
            return item.children.count
        } else {
            return self.data.count
        }
    }
    
    //MARK: RATreeViewDelegate Methods
    public func treeView(_ treeView: RATreeView, child index: Int, ofItem item: Any?) -> Any {
        if let item = item as? DataObject {
            return item.children[index]
        } else {
            return data[index] as AnyObject
        }
    }
    
    public func treeView(_ treeView: RATreeView, heightForRowForItem item: Any) -> CGFloat {
        
        let item = item as! DataObject
        if !(item.topCellObject.isEmpty) || item.children.count > 0 {
            return 75
        }
        else {
            return 150
        }
    }
        
    public func treeView(_ treeView: RATreeView, didSelectRowForItem item: Any) {
        
        let tappedItem = item as! DataObject
        let topCellIndex = tappedItem.topCellObject
        var  imageArray = NSArray()
        
        //detects if bottom cell tapped
        if !(topCellIndex.isEmpty) {
        
            let tappedItem1 = topCellIndex[0]

            print("tappedItem1 name == \(tappedItem1.name)")
            print("topCellObject name == \(tappedItem.topCellObject)")
            
            let cell = treeView.cell(forItem: tappedItem1)
        
            if cell == nil {
                treeView.collapseRow(forItem: tappedItem1, collapseChildren: true, with: RATreeViewRowAnimationTop)
                treeView.collapseRow(forItem: tappedItem1.children, collapseChildren: true, with: RATreeViewRowAnimationTop)
            }
            else {
                if treeView.isCellExpanded(cell!) {
                    treeView.collapseRow(forItem: tappedItem1)}
                else {
                    treeView.expandRow(forItem: tappedItem1)
                }
            }

               imageArray = populateImageArray(itemName: tappedItem1.name)
        }
        else {
              imageArray = populateImageArray(itemName: tappedItem.name)
         
        }
        MainViewController?.treeDidUpdate(imageArray: imageArray)
    }
    
    //MARK: Helper Methods
    func populateImageArray(itemName:String) -> NSArray
    {
        let imageArray = NSMutableArray()
        var number = 1
        var imageName = "\(itemName)-image-\(number)" // 1.1_image-1
        
        while checkIfImageExists(imageName: imageName) {
            imageArray.add(imageName)
            number += 1
            imageName = "\(itemName)-image-\(number)"
        }

        return imageArray
    }
    
    func checkIfImageExists(imageName:String) -> Bool
    {
        let image = UIImage(named:imageName)
        if image != nil {
            return true
        }
        else {
            return false
        }
    }
    
    func setupContentView()
    {
        //make this dynamic
        let rect = CGRect(origin: CGPoint(x: 200,y :-30), size: CGSize(width: view.bounds.width - 250, height: view.bounds.height - 100))
        
        contentImageView = UIImageView(frame: rect)
        contentImageView.contentMode = UIViewContentMode.center
        contentImageView.backgroundColor = UIColor.clear
    }
    
 func treeView(_ treeView: RATreeView, editingStyleForRowForItem item: Any) -> UITableViewCellEditingStyle {
    return .none
    }
    
    func setupTreeView() -> Void {
        
        treeView = RATreeView(frame:self.view.frame)
        treeView.register(UINib.init(nibName: "GBTreeViewCell", bundle: nil), forCellReuseIdentifier: "GBTreeViewCell")
        treeView.autoresizingMask = UIViewAutoresizing(rawValue:UIViewAutoresizing.flexibleWidth.rawValue | UIViewAutoresizing.flexibleHeight.rawValue)
        treeView.delegate = self;
        treeView.dataSource = self;
        treeView.treeFooterView = UIView()
        treeView.backgroundColor = UIColor.clear
        treeView.separatorStyle = RATreeViewCellSeparatorStyleNone
        treeView.collapsesChildRowsWhenRowCollapses = true
        treeView.isEditing = false
        view.addSubview(treeView)
        
        //adding label
        let labelFrame = CGRect(origin:CGPoint(x: self.view.center.x,y :300), size: CGSize(width: 200, height: 100))
        touchLabel = UILabel(frame:labelFrame)
        touchLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        touchLabel.textColor = UIColor.black
        self.view.addSubview(touchLabel)
    }
    
    func didTapThumbnail()
    {
        print("thumbnail tapped")
    }
}

private extension GBTreeViewController {
    
    //Parsing the JSON structure
    static func fillNodeWithChildrenArray(children: [JSON]) -> [DataObject]
    {
        let childrenArray = NSMutableArray()
        
        for m in 0..<children.count {
            let Dictionary = children[m].dictionary
            
            if let children2 = Dictionary?["children"]?.array
            {
                let name = "\(Dictionary!["id"]!)"
                
                //if node has children
                if !(children2.isEmpty) {
                    let lvl1Object = DataObject(name: "\(name)", children: GBTreeViewController.fillNodeWithChildrenArray(children: children2), topCellObject: [])
                    childrenArray.add(lvl1Object)
                    
                    let ObjectBottom = DataObject(name: "\(name)", topCellObject: [lvl1Object])
                    childrenArray.add(ObjectBottom)
                }
                    //if node does not have children
                else {
                    let lvl1Object = DataObject(name: name)
                    childrenArray.add(lvl1Object)
                }
            }
        }
        
        let cArray = childrenArray as NSArray as! [DataObject]
        return cArray
        
    }
    static func parseJSON() -> [DataObject] {
        
        //init jason data
        let path = Bundle.main.path(forResource: "structure", ofType: "json")
        let jsonData = NSData(contentsOfFile:path!)
        let json = JSON(data: jsonData! as Data)
        let array = json.array! as NSArray
        let lvl0array = NSMutableArray()
        
        for i in 0..<array.count {
            
            let lvl0Dictionary = json.array?[i].dictionary
            if let children = lvl0Dictionary?["children"]?.array {
                
                let name = "\(lvl0Dictionary!["id"]!)" as String
                
                if !(children.isEmpty) {
                    let lvl0Object = DataObject(name: "\(name)", children: GBTreeViewController.fillNodeWithChildrenArray(children: children), topCellObject: [])
                    lvl0array.add(lvl0Object)
                    
                    let ObjectBottom = DataObject(name: "\(name)", topCellObject:[lvl0Object])
                    lvl0array.add(ObjectBottom)
                }
                else {
                    let lvl0Object = DataObject(name: name, children: GBTreeViewController.fillNodeWithChildrenArray(children: children), topCellObject: [])
                    lvl0array.add(lvl0Object)
                }
            }
        }
        
        let finalArray = lvl0array as NSArray as! [DataObject]
        return finalArray
        
    }
}

extension UIImage {
    
    var topHalf: UIImage? {
        
        let imageRef = self.cgImage!.cropping(to: CGRect(origin: CGPoint(x: 0, y: 0),
                                                         size: CGSize(width: self.size.width*2, height: self.size.height)))
        
        let image = UIImage(cgImage: imageRef!, scale:1.0, orientation: self.imageOrientation)
        
        return image
    }
    
    var bottomHalf: UIImage? {
        
        let imageRef = self.cgImage!.cropping(to: CGRect(origin: CGPoint(x: 0,  y: 145),
                                                         size: CGSize(width: self.size.width*2, height: self.size.height)))
        
        let image = UIImage(cgImage: imageRef!, scale: 1.0, orientation: self.imageOrientation)
        return image
        
    }
    
}


