//
//  GBCarouselViewController.swift
//  Glassbooks
//
//  Created by Abdullah Kahn on 2/8/17.
//  Copyright © 2017 Glassbooks. All rights reserved.
//

import UIKit

protocol CarousalDelegate {
    func didTapThumbnail()
}

private let reuseIdentifier = "carouselCell"

class GBCarouselViewController: UICollectionViewController {
    
    var delegate:CarousalDelegate?
    var imageArray = NSMutableArray()
    var MainViewController: GBMainViewController?

    
    override func didMove(toParentViewController parent: UIViewController?)
    {
        super.didMove(toParentViewController: parent)
        
        MainViewController = parent as? GBMainViewController
    }
    
    static let sharedInstance = GBCarouselViewController()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        imageArray.addObjects(from:["1.1.2-image-2","1.1.2-image-3","1.1.2-image-4","1.1.2-image-5","1.1.2-image-6","1.1.2-image-7"])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsetsMake(0, 20, 0, 20)
        
        self.collectionView?.collectionViewLayout = layout
    }
    
    func reloadCollectionView(array:NSArray)
    {
        print("collection reloaded")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("reload count \(imageArray.count)")
        
        return imageArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "carouselCell", for: indexPath) as! GBCarouselCell
        
        cell.imageView.image = UIImage(named: imageArray[indexPath.row] as! String)
        
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        MainViewController?.carouselDidUpdate(imageName: imageArray[indexPath.row] as! String)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 100, height: 100)
    }
    
}
