//
//  GBTreeViewCell.swift
//  Glassbooks
//
//  Created by Abdullah Kahn on 2/8/17.
//  Copyright © 2017 Glassbooks. All rights reserved.
//

import UIKit

class GBTreeViewCell : UITableViewCell {
    
    @IBOutlet var cellImageView: UIImageView!
    
    override func awakeFromNib() {
        selectedBackgroundView? = UIView.init()
        selectedBackgroundView?.backgroundColor = UIColor.clear
    }
    
    var additionButtonActionBlock : ((GBTreeViewCell) -> Void)?;
    
    func setup(withTitle title: String, detailsText: String, level : Int, additionalButtonHidden: Bool) {
        
        self.backgroundColor = UIColor.clear
        
        // if level == 0 {
    }
    
}
